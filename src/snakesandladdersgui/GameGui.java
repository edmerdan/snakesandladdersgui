package snakesandladdersgui;

import games.snakesandladders.GameBoard;
import games.snakesandladders.Player;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.security.SecureRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

public class GameGui implements Runnable {
    private final BufferedImage img;
    //private Thread thread;
    private final JFrame f;
    private final Canvas c;
    private final int width;
    private final int height;
    private final int tilesSquared;
    private volatile boolean repaint;
    private final SecureRandom random;
    private final Player[] players;
    private final GameBoard board;
    
    // TODO: Implement a games.snakesandladders.GameReporterAbstract in order to
    // more properly display actions, rather than using the raw player positions
    // directly.
    public GameGui(GameBoard board, Player[] players, int width, int height, int tilesSquared) {
        this.width = width;
        this.height = height;
        this.tilesSquared = tilesSquared;
        this.repaint = true;
        this.players = players;
        this.board = board;
        this.random = new SecureRandom();
        
        img = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
        
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gd = ge.getScreenDevices()[0];
        GraphicsConfiguration gc = gd.getConfigurations()[0];
        
        c = new Canvas(gc);
        c.setIgnoreRepaint(true);

        f = new JFrame("Render_Prototype");
        f.setUndecorated(true);
        f.setIgnoreRepaint(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.add(c);
        f.pack();
        f.setSize(width, height);
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }
    
    public void begin() throws Exception {
        // TODO: Proper event-driven threading for repaints instead of this
        // loop... or be lazy and keep it
        while(true) {
            if (!repaint) continue;
            
            c.createBufferStrategy(2);
            BufferStrategy bs = c.getBufferStrategy();
            Graphics g = bs.getDrawGraphics();
                        
            this.drawBoard(g, bs, 0, 0, this.width, this.height);
            
            //this.repaint = false;
            
            try {
                Thread.sleep(16, 666);
            } catch (InterruptedException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void drawBoard(Graphics g, BufferStrategy bs, int boardX, int boardY, int boardWidth, int boardHeight) throws Exception {
        Font unsetFont = g.getFont();
        
        g.setFont(new Font("Stencil", Font.PLAIN, 12));
        ((Graphics2D)g).addRenderingHints(new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON));
        ((Graphics2D)g).addRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));

        int row = 0;
        int col = 0;
        for(int i = this.tilesSquared * this.tilesSquared; i > 0; i--) {
            if (!(row == 0 && col == 0) && i % this.tilesSquared == 0) {
                row++;
                col = 0;
            }
            drawTile(g,
                    boardX + col * (boardWidth / this.tilesSquared),
                    boardY + row * (boardHeight / this.tilesSquared),
                    boardWidth / this.tilesSquared,
                    boardHeight / this.tilesSquared,
                    i);
            col++;
        }

        g.dispose();
        bs.show();
        
        g.setFont(unsetFont);
    }
    
    private void drawTile(Graphics g, int tileX, int tileY, int tileWidth, int tileHeight, int i) throws Exception {
        Color color = Color.GRAY;

        String text = ""+i;
        g.drawImage(img, tileX, tileY, tileWidth, tileHeight, color, null);
        
        this.drawPlayer(g, tileX, tileY, tileWidth, tileHeight, i);
        
        FontMetrics fm = g.getFontMetrics();
        
        int circleCenterX = (int)Math.round(tileX + tileWidth / 2 - 32 / 2);
        int circleCenterY = (int)Math.round(tileY + tileHeight / 2 - 32 / 2);
        
        // Calculating length before appending text was an accident at first,
        // but it actually looks better (as a placeholder, anyhow)
        int fontCenterX = (int)Math.round(tileX + tileWidth / 2 - fm.stringWidth(text) / 2);
        int fontCenterY = (int)Math.round(fm.getAscent() + (tileY + tileHeight / 2 - (fm.getAscent() + fm.getDescent()) / 2));
        
        g.setColor(new Color(255, 255, 255, 127));
        g.fillOval(circleCenterX, circleCenterY, 32, 32);
        
        if (this.board.getBoard()[i - 1] == 0) {
            g.setColor(Color.BLACK);            
        } else if (this.board.getBoard()[i - 1] > 0) {
            g.setColor(Color.GREEN);
            text += "->" + (i + this.board.getBoard()[i - 1]);
        } else if (this.board.getBoard()[i - 1] < 0) {
            g.setColor(Color.RED);
            text += "->" + (i + this.board.getBoard()[i - 1]);
        }
        
        g.drawString(text, fontCenterX, fontCenterY);
    }
    
    private void drawPlayer(Graphics g, int tileX, int tileY, int tileWidth, int tileHeight, int i) throws Exception {
        Color color = Color.PINK;

        for (int k = 0; k < this.players.length; k++) {
            if (this.players[k].getPosition() + 1 != i) continue;
            
            // TODO: Random complimentary distance colours
            switch(k) {
                case 0:
                    color = Color.RED;
                    break;
                case 1:
                    color = Color.GREEN;
                    break;
                case 2:
                    color = Color.BLUE;
                    break;
                case 3:
                    color = Color.CYAN;
                    break;
                case 4:
                    color = Color.MAGENTA;
                    break;
                case 5:
                    color = Color.YELLOW;
                    break;
                case 6:
                    color = Color.BLACK;
                    break;
                case 7:
                    color = Color.WHITE;
                    break;
                case 8:
                    color = Color.ORANGE;
                    break;
                case 9:
                    color = Color.PINK;
                    break;
                case 10:
                    throw new Exception("More than 10 players is not supported.");
                
            }
            g.drawImage(img,
                    tileX + (tileWidth / this.players.length) * k,
                    tileY,
                    tileWidth / this.players.length,
                    tileHeight,
                    color, null);
        }
    }
    
    @Override
    public void run() {
        try {
            this.begin();
        } catch (Exception ex) {
            // As I understand it, it is not possible to throw an exception and
            // then catch it outside of its current thread in Java.
            Logger.getLogger(GameGui.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
