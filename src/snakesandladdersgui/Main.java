package snakesandladdersgui;

import games.snakesandladders.Die;
import games.snakesandladders.DummyGameReporter;
import games.snakesandladders.Game;
import games.snakesandladders.GameBoard;
import games.snakesandladders.GameState;
import games.snakesandladders.Player;
import games.snakesandladders.Rules;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        // TODO: User input for players' names
        Player[] players = {
            new Player("Edmond Dantés"),
            new Player("Giovanni Bertuccio"),
            new Player("Gérard de Villefort"),
            new Player("Baron Danglars"),
            new Player("Mercédès"),
            new Player("Fernand Mondego"),
            new Player("Abbé Faria"),
            new Player("Caderousse"),
            new Player("Benedetto"),
        };
        
        Die die = new Die(6);
        GameBoard board = new GameBoard(100);
        board.addLadder(0, 37);
        board.addLadder(3, 10);
        board.addLadder(8, 22);
        board.addLadder(20, 21);
        board.addLadder(27, 56);
        board.addLadder(35, 8);
        board.addLadder(50, 16);
        board.addLadder(70, 20);
        board.addLadder(79, 20);
        board.addSnake(15, 10);
        board.addSnake(46, 21);
        board.addSnake(48, 38);
        board.addSnake(55, 3);
        board.addSnake(63, 4);
        board.addSnake(86, 63);
        board.addSnake(92, 20);
        board.addSnake(94, 20);
        board.addSnake(97, 20);
        
        Game game = new Game(board, players, die, new DummyGameReporter(), new Rules());
        game.startGame();
        
        GameGui gui = new GameGui(board, players, 800, 600, 10);
        Thread guiThread = new Thread(gui);
        guiThread.start();
        
        while(game.getState() != GameState.COMPLETED) {
            // TODO: Allow players to "roll" and advance the game on their own,
            // rather than wait 1 second between turns
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            game.nextTurn();
        }
    }
}